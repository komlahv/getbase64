const mimeType = async (base64String) => {
  const base64DataArray = base64String.split(',');
  const mime = base64DataArray[0].match(/:(.*?);/)[1];
  return mime;
}

module.exports = mimeType