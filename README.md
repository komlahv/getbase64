# Thank you for using getBase64

## Get base64 data and mime types with ease

import getBase64 from 'getbase64data'

const base64Data = await getbase64.fromFile(file)

const base64Data = await getBase64.fromURL(url)

const mimeType = await getBase64.mimeType(base64String)

## Examples

From file (React snippet)

    const  handleImageUpload  =  async  (file)  => {
        const  base64  =  await getBase64.fromFile(file)
        console.log(base64)
    };

    <input  type="file"  id="img"  name="img"  accept="image/*"  onChange={(e) => handleImageUpload(e.target.files[0])}/>

From URL (React snippet)

    const  handleUrlToBase64  =  async  (event)  => {
        const  url  = event.target.value
        const  base64  =  await getBase64.fromURL(url)
        console.log(base64)
    };

    <input  type='text'  id="url"  name="url"  onChange={handleUrlToBase64}/>

Mime type from base64 string

    const mimeType = await getBase64.mimeType(base64String)
