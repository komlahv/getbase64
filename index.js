const fromFile = require('./fromFile')
const fromURL = require('./fromURL')
const mimeType = require('./mimeType')

const getBase64 = {
  fromFile,
  fromURL,
  mimeType
}

module.exports = getBase64